# test_project_weather

to up project, run: "docker-compose up --scale worker=3"

then make connections: 
1) Conn Id : geo_helper_default
   Host : http://geohelper.info
   Extra : {"apiKey":"1GGPiFKwZHW6Ctyg5yETno4TV74c0sHD"}
   
2) Conn Id: weather_list_default 
Host: https://community-open-weather-map.p.rapidapi.com
Extra: 
{"apiKey": "5a6d1fb7femsh005faa8e7d228c1p191d89jsnfea1fd68d49c","x-rapidapi-host": "community-open-weather-map.p.rapidapi.com"}
   
in dags, I put a limit on receiving cities, since the api for receiving weather has a limit on requests for the free version

get_city_list = GetCityList(
    geo_helper_conn_id='geo_helper_default',
    city_limit=5,
    task_id='get_city_list',
    dag=dag
)


airflow address: http://127.0.0.1:8080/
