from datetime import timedelta, datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from commons.operators import GetCityList, GetWeatherList, MakeReport

dag = DAG('make_weather_report',
          schedule_interval=timedelta(hours=6),
          start_date=datetime(2020, 7, 8, 0),
          default_args={'retries': 3, 'retry_delay': timedelta(seconds=10)})

start_process = DummyOperator(task_id='start_process', dag=dag)

get_city_list = GetCityList(
    geo_helper_conn_id='geo_helper_default',
    city_limit=5,
    task_id='get_city_list',
    dag=dag
)
get_weather = GetWeatherList(
    weather_list_conn_id='weather_list_default',
    file_to_save='weather.json',
    cities_file='cities_list.json',
    task_id='get_weather',
    dag=dag
)
make_report = MakeReport(
    task_id='make_report',
    weather_file='weather.json',
    report_file='report.csv',
    dag=dag
)

end_process = DummyOperator(task_id='end_process', dag=dag)

start_process >> get_city_list >> get_weather >> make_report >> end_process
