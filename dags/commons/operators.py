from airflow.operators import BaseOperator
from commons.hooks import CityListHook, GeoHelper, WeatherListHook, WeatherHelper
from pathlib import Path
import json
import csv
from datetime import datetime


class GetCityList(BaseOperator):

    def __init__(self,
                 geo_helper_conn_id: str = 'geo_helper_default',
                 file_to_save: str = 'cities_list.json',
                 city_limit: int = 5,
                 *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._hook = CityListHook(geo_helper_conn_id=geo_helper_conn_id)
        self.client: GeoHelper = self._hook.client
        self.file_path = "{}/{}".format(Path(__file__).resolve().parent.parent, file_to_save)
        self.city_limit = city_limit

    def execute(self, context):
        page = 1
        result = []
        move = True
        while move:
            response = self.client.get_cities_list(page=page)
            for item in response['result']:
                if item['localityType']['code'] == 'city-city':
                    result.append(item['localizedNames']['en'] + ",ua")
                if len(result) > self.city_limit-1:
                    move = False
                    break
            if int(response['pagination']['totalPageCount']) <= page:
                break
            page = page + 1
        f = open(self.file_path, "w")
        f.write(json.dumps(result))
        f.close()


class GetWeatherList(BaseOperator):

    def __init__(self,
                 weather_list_conn_id: str = 'weather_list_default',
                 file_to_save: str = 'weather.json',
                 cities_file: str = 'cities_list.json',
                 *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._hook = WeatherListHook(weather_list_conn_id=weather_list_conn_id)
        self.client: WeatherHelper = self._hook.client
        self.file_path = "{}/{}".format(Path(__file__).resolve().parent.parent, file_to_save)
        self.cities_list_file = "{}/{}".format(Path(__file__).resolve().parent.parent, cities_file)

    def execute(self, context):
        f = open(self.cities_list_file, "r")
        cities = json.loads(f.read())
        f.close()
        result = []
        now = datetime.now()
        for city in cities:
            response = self.client.get_weather(city=city)
            result.append(
                {
                    'city': city,
                    'temperature': response['main']['temp'],
                    'humidity': response['main']['humidity'],
                    'date': now.strftime("%m/%d/%Y")
                }
            )
        f = open(self.file_path, "w")
        f.write(json.dumps(result))
        f.close()


class MakeReport(BaseOperator):
    def __init__(self,
                 weather_file: str = 'weather.json',
                 report_file: str = 'report.csv',
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.weather_file = "{}/{}".format(Path(__file__).resolve().parent.parent, weather_file)
        self.report_file = "{}/{}".format(Path(__file__).resolve().parent.parent, report_file)

    def execute(self, context):
        with open(self.weather_file) as weather_file:
            weather = json.load(weather_file)
        report_file = open(self.report_file, 'a')
        report_writer = csv.writer(report_file)

        count = 0

        for city_weather in weather:
            if count == 0:
                header = city_weather.keys()
                report_writer.writerow(header)
                count += 1
            report_writer.writerow(city_weather.values())
        report_file.close()
