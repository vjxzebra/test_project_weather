from airflow.hooks.base_hook import BaseHook
import requests
from airflow.models.connection import Connection
import json
from pathlib import Path


class CityListHook(BaseHook):
    def __init__(self,
                 geo_helper_conn_id='geo_helper_default', *args, **kwargs):
        super().__init__(geo_helper_conn_id)

        self.geo_helper_conn_id = geo_helper_conn_id
        self.geo_helper_api_key = None
        self.client = None
        self.get_conn()

    def get_conn(self):
        connection = self.get_connection(self.geo_helper_conn_id)
        self.client = GeoHelper(connection)
        return self.client


class GeoHelper:
    API_ENDPOINT = '{}/api/v1/cities?apiKey={}&locale[lang]=ru&filter[countryIso]=ua'

    def __init__(self, connection: Connection):
        self._base_url = GeoHelper.API_ENDPOINT.format(connection.host, connection.extra_dejson['apiKey'])
        self.page_by = 100

    def get_cities_list(self, pagination_limit: int = 100, page: int = 1):

        url = self._base_url+"&pagination[limit]={}&pagination[page]={}".format(
            pagination_limit or self.page_by,
            page or 1
        )
        response = requests.request("GET", url).json()
        if not response['success']:
            raise GeoHelperException(response)
        return response


class GeoHelperException(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__((args, kwargs))


class WeatherListHook(BaseHook):
    def __init__(self,
                 weather_list_conn_id='weather_list_default', *args, **kwargs):
        super().__init__(weather_list_conn_id)

        self.weather_list_conn_id = weather_list_conn_id
        self.weather_list_api_key = None
        self.client = None
        self.get_conn()

    def get_conn(self):
        connection = self.get_connection(self.weather_list_conn_id)
        self.client = WeatherHelper(connection)
        return self.client


class WeatherHelper:

    API_ENDPOINT = '{}/weather'

    def __init__(self, connection: Connection):
        self.headers = {
            'x-rapidapi-key': "{}".format(connection.extra_dejson['apiKey']),
            'x-rapidapi-host': "{}".format(connection.extra_dejson['x-rapidapi-host'])
        }
        self._base_url = WeatherHelper.API_ENDPOINT.format(connection.host)

    def get_weather(self, city: str):
        url = self._base_url

        querystring = {"q": city, "lang": "ua",
                       "units": "metric"}

        response = requests.request("GET", url, headers=self.headers, params=querystring)

        result = response.json()
        if not result['cod'] == 200:
            raise WeatherHelperException(result)
        return result


class WeatherHelperException(Exception):
    def __init__(self, *args, **kwargs):
        super().__init__((args, kwargs))